<?php declare(strict_types=1);

namespace DaveRandom\WwwAdminTools\BinWrappers;

final class PasswordWrapper extends Wrapper
{
    public function __construct(string $binDirectory = null)
    {
        parent::__construct($binDirectory);
    }

    public function setPassword(string $user, string $password): void
    {
        $this->runSerializedCommand('password', ['--serialize-output', 'set', $user], $password);
    }

    public function verifyPassword(string $user, string $password): bool
    {
        return (bool)$this->runSerializedCommand('password', ['--serialize-output', 'verify', $user], $password);
    }

    public function generatePassword(int $length = null): string
    {
        return (string)$this->runSerializedCommand('password', ['--serialize-output', 'generate', $length]);
    }
}

<?php declare(strict_types=1);

namespace DaveRandom\WwwAdminTools\BinWrappers;

abstract class Wrapper
{
    private $binDirectory;

    protected function __construct(string $binDirectory = null)
    {
        $this->binDirectory = $binDirectory ?? __DIR__ . '/../../bin';

        if (!\is_dir($this->binDirectory)) {
            throw new \LogicException("Binaries directory {$this->binDirectory} does not exist or is not a directory");
        }
    }

    final protected function runSerializedCommand(string $command, array $args, string $input = null)
    {
        $safeArgs = [];

        foreach ($args as $arg) {
            if ($arg !== null) {
                $safeArgs[] = \escapeshellarg(\trim((string)$arg));
            }
        }

        $command = \sprintf("'%s/%s' %s", $this->binDirectory, $command, \implode(' ', $safeArgs));

        $handle = \proc_open($command, [['pipe', 'r'], ['pipe', 'w'], ['pipe', 'w']], $pipes);
        if ($handle === false) {
            throw new \RuntimeException("Failed to execute '{$command}': starting child process failed");
        }

        if ($input !== null && $input !== '' && \fwrite($pipes[0], $input) !== \strlen($input)) {
            throw new \RuntimeException("Failed to execute '{$command}': sending data to child process failed");
        }

        \fclose($pipes[0]);

        $stdOut = \trim(\stream_get_contents($pipes[1]));
        \fclose($pipes[1]);

        $stdErr = \trim(\stream_get_contents($pipes[2]));
        \fclose($pipes[2]);

        $status = \proc_close($handle);

        if ($status === 0) {
            $output = @\unserialize($stdOut);

            if ($output === false && $stdOut !== 'b:0;') {
                throw new \RuntimeException("Failed to execute '{$command}': unserializing output failed: '{$stdOut}'");
            }

            return $output;
        }

        $error = @\unserialize($stdErr);

        if ($error === false && $stdErr !== 'b:0;') {
            throw new \RuntimeException("Failed to execute '{$command}': unserializing error failed: '{$stdErr}'");
        }

        if ($error instanceof \Throwable) {
            throw new \RuntimeException("Failed to execute '{$command}': {$error->getMessage()}", $status, $error);
        }

        throw new \RuntimeException("Failed to execute '{$command}': {$error}");
    }
}

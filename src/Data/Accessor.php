<?php declare(strict_types=1);

namespace DaveRandom\WwwAdminTools\Data;

class Accessor
{
    private const DEFAULT_DATA_FILE = \DaveRandom\WwwAdminTools\DATA_ROOT . '/hosts.json';

    private $fileHandle;

    public function __construct(string $dataFile = self::DEFAULT_DATA_FILE)
    {
        $this->fileHandle = new JsonFileHandle($dataFile);
    }

    public function transaction(int $mode, callable $callback, ...$args)
    {
        return $this->fileHandle->withLock($mode, $callback, ...$args);
    }

    public function storeHost(Host $host): void
    {
        $this->fileHandle->withLock(\LOCK_EX, [$this->fileHandle, 'write'], function($data) use($host) {
            $primaryDomain = $host->getPrimaryDomain();

            $aliasMap = [];

            foreach ($host->getAliasDomains() as $alias) {
                $data['aliases']['forward'][$alias] = $aliasMap[$alias] = $primaryDomain;
            }

            foreach ($data['aliases']['reverse'][$primaryDomain] ?? [] as $alias) {
                if (!isset($aliasMap[$alias])) {
                    unset($data['aliases']['forward'][$alias]);
                }
            }

            $data['aliases']['reverse'][$primaryDomain] = $host->getAliasDomains();
            $data['hosts'][$primaryDomain] = $host;
            $data['users'][$host->getUser()->getUsername()] = $host->getUser();

            return $data;
        });
    }

    public function deleteHost(string $domain): void
    {
        $this->fileHandle->withLock(\LOCK_EX, [$this->fileHandle, 'write'], function($data) use($domain) {
            if (!isset($data['hosts'][$domain])) {
                throw new \LogicException("Non-existent host: {$domain}");
            }

            foreach ($data['aliases']['reverse'][$domain] ?? [] as $alias) {
                unset($data['aliases']['forward'][$alias]);
            }

            $username = $data['hosts'][$domain]['user'];
            unset($data['aliases']['reverse'][$domain], $data['hosts'][$domain], $data['users'][$username]);

            return $data;
        });
    }

    public function domainExists(string $domain): bool
    {
        $data = $this->fileHandle->withLock(\LOCK_SH, [$this->fileHandle, 'read']);

        if (isset($data['hosts'][$domain])) {
            return true;
        }

        if (!isset($data['aliases']['forward'][$domain])) {
            return false;
        }

        \assert(
            isset($data['hosts'][$data['aliases']['forward'][$domain]]),
            new \LogicException("Alias {$domain} points to non-existent host {$data['aliases']['forward'][$domain]}")
        );

        return true;
    }

    public function getHost(string $domain): Host
    {
        $data = $this->fileHandle->withLock(\LOCK_SH, [$this->fileHandle, 'read']);

        if (isset($data['hosts'][$domain])) {
            $host = $data['hosts'][$domain];
        } else if (isset($data['aliases']['forward'][$domain])) {
            \assert(
                isset($data['hosts'][$data['aliases']['forward'][$domain]]),
                new \LogicException("Alias {$domain} points to non-existent host {$data['aliases']['forward'][$domain]}")
            );

            $host = $data['hosts'][$data['aliases']['forward'][$domain]];
        } else {
            throw new \LogicException("Non-existent host: {$domain}");
        }

        \assert(
            isset($data['users'][$host['user']]),
            new \LogicException("Host {$host['primary_domain']} points to non-existent user {$host['user']}")
        );

        $user = User::createFromJson($data['users'][$host['user']]);

        return Host::createFromJson($host, $user);
    }

    public function storeUser(User $user): void
    {
        $this->fileHandle->withLock(\LOCK_EX, [$this->fileHandle, 'write'], function($data) use($user) {
            $data['users'][$user->getUsername()] = $user;
            return $data;
        });
    }

    public function userExists(string $username): bool
    {
        $data = $this->fileHandle->withLock(\LOCK_SH, [$this->fileHandle, 'read']);
        return isset($data['users'][$username]);
    }

    public function getUser(string $username): User
    {
        $data = $this->fileHandle->withLock(\LOCK_SH, [$this->fileHandle, 'read']);

        if (!isset($data['users'][$username])) {
            throw new \LogicException("Non-existent user: {$username}");
        }

        return User::createFromJson($data['users'][$username]);
    }
}

<?php declare(strict_types=1);

namespace DaveRandom\WwwAdminTools\Data;

interface Entity extends \JsonSerializable
{
    function __toString(): string;
}

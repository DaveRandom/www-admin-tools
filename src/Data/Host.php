<?php declare(strict_types = 1);

namespace DaveRandom\WwwAdminTools\Data;

use DaveRandom\WwwAdminTools\Owner;

final class Host implements Entity
{
    private const DIRECTORY_TREE_LAYOUT = [
        'conf' => [0755, Owner::ROOT, Owner::ROOT],
        'logs' => [0775, Owner::ROOT, Owner::WEB_SERVER],
        'docroot' => [0755, Owner::DOMAIN, Owner::DOMAIN],
    ];

    private $primaryDomain;
    private $aliasDomains;
    private $baseDirectory;
    private $user;

    public function jsonSerialize(): array
    {
        return [
            'primary_domain' => $this->primaryDomain,
            'alias_domains'  => $this->aliasDomains,
            'base_directory' => $this->baseDirectory,
            'user'           => $this->user->getUsername(),
        ];
    }

    public function __toString(): string
    {
        return $this->primaryDomain;
    }

    public static function createFromJson(array $values, User $user): self
    {
        return new self(
            (string)$values['primary_domain'],
            (array)$values['alias_domains'],
            (string)$values['base_directory'],
            $user
        );
    }

    /**
     * @param string $primaryDomain
     * @param string[] $aliasDomains
     * @param string $baseDirectory
     * @param User $user
     */
    public function __construct(string $primaryDomain, array $aliasDomains, string $baseDirectory, User $user)
    {
        $this->primaryDomain = $primaryDomain;
        $this->aliasDomains = \array_map('strval', $aliasDomains);
        $this->baseDirectory = $baseDirectory;
        $this->user = $user;
    }

    public function getPrimaryDomain(): string
    {
        return $this->primaryDomain;
    }

    /**
     * @return string[]
     */
    public function getAliasDomains(): array
    {
        return $this->aliasDomains;
    }

    /**
     * @return string[]
     */
    public function getAllDomains(): array
    {
        return \array_merge([$this->primaryDomain], $this->aliasDomains);
    }

    public function getBaseDirectory(): string
    {
        return $this->baseDirectory;
    }

    public function getDocRootDirectory(): string
    {
        return $this->baseDirectory . '/public';
    }

    public function getLogsDirectory(): string
    {
        return $this->baseDirectory . '/logs';
    }

    public function getConfDirectory(): string
    {
        return $this->baseDirectory . '/conf';
    }

    public function getDirectoryLayout(): array
    {
        $result = [];

        foreach (self::DIRECTORY_TREE_LAYOUT as $type => $options) {
            $result[$this->{"get{$type}Directory"}()] = $options;
        }

        return $result;
    }

    public function getUser(): User
    {
        return $this->user;
    }
}

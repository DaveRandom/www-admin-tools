<?php declare(strict_types=1);

namespace DaveRandom\WwwAdminTools\Data;

class JsonFileHandle
{
    private $path;
    private $fp;

    private $dataCache;

    private $lockDepth = 0;
    private $sharedLockLevel;
    private $exclusiveLockLevel;

    public function __construct(string $path)
    {
        $this->path = $path;
        $this->fp = \fopen($path, \file_exists($path) ? 'r+' : 'w+');

        if ($this->fp === false) {
            throw new \RuntimeException("Unable to open data file {$path}");
        }
    }

    private function acquireExclusiveLock(): void
    {
        if ($this->exclusiveLockLevel !== null) {
            $this->lockDepth++;
            return;
        }

        if (!\flock($this->fp, \LOCK_EX)) {
            throw new \RuntimeException("Failed to acquire exclusive lock on {$this->path}");
        }

        $this->exclusiveLockLevel = ++$this->lockDepth;

        if ($this->sharedLockLevel === null) {
            $this->sharedLockLevel = $this->lockDepth;
        }
    }

    private function acquireSharedLock(): void
    {
        if ($this->sharedLockLevel !== null) {
            $this->lockDepth++;
            return;
        }

        if (!\flock($this->fp, \LOCK_SH)) {
            throw new \RuntimeException("Failed to acquire shared lock on {$this->path}");
        }

        $this->sharedLockLevel = ++$this->lockDepth;
    }

    private function releaseExclusiveLock(): void
    {
        if ($this->exclusiveLockLevel === $this->sharedLockLevel) {
            if (!\flock($this->fp, \LOCK_UN)) {
                throw new \RuntimeException("Failed to release all locks on {$this->path}");
            }

            $this->sharedLockLevel = $this->dataCache = null;
        } else if (!\flock($this->fp, \LOCK_SH)) {
            throw new \RuntimeException("Failed to release exclusive lock on {$this->path}");
        }

        $this->lockDepth--;
        $this->exclusiveLockLevel = null;
    }

    private function releaseSharedLock(): void
    {
        if (!\flock($this->fp, \LOCK_UN)) {
            throw new \RuntimeException("Failed to release shared lock on {$this->path}");
        }

        $this->lockDepth--;
        $this->sharedLockLevel = $this->dataCache = null;
    }

    public function withLock(int $mode, callable $callback, ...$args)
    {
        if ($mode === \LOCK_EX) {
            $this->acquireExclusiveLock();
        } else if ($mode === \LOCK_SH) {
            $this->acquireSharedLock();
        } else {
            throw new \LogicException("Invalid lock mode: {$mode}");
        }

        try {
            return $callback(...$args);
        } finally {
            if ($this->exclusiveLockLevel === $this->lockDepth) {
                $this->releaseExclusiveLock();
            } else if ($this->sharedLockLevel === $this->lockDepth) {
                $this->releaseSharedLock();
            } else {
                $this->lockDepth--;
            }
        }
    }

    public function read(): array
    {
        if ($this->dataCache !== null) {
            return $this->dataCache;
        }

        if (!\rewind($this->fp)) {
            throw new \RuntimeException("Failed to rewind pointer to beginning of {$this->path}");
        }

        if (false === $data = \stream_get_contents($this->fp)) {
            throw new \RuntimeException("Failed to read data file {$this->path}");
        }

        return $this->dataCache = $data !== ''
            ? \ExceptionalJSON\decode($data, true)
            : [];
    }

    public function write(callable $callback): int
    {
        $data = $callback($this->read());
        $encoded = \ExceptionalJSON\encode($data);
        $len = \strlen($encoded);

        if (!\rewind($this->fp)) {
            throw new \RuntimeException("Failed to rewind pointer to beginning of {$this->path}");
        }

        if (!\ftruncate($this->fp, 0)) {
            throw new \RuntimeException("Failed to truncate {$this->path}");
        }

        if (\fwrite($this->fp, $encoded) !== $len) {
            throw new \RuntimeException("Failed to write data to {$this->path}");
        }

        $this->dataCache = $data;

        return $len;
    }
}

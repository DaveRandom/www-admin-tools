<?php declare(strict_types = 1);

namespace DaveRandom\WwwAdminTools\Data;

final class Password implements Entity
{
    private const ALGORITHM = \PASSWORD_DEFAULT;

    private $value;
    private $hashedValue;

    private function getHashedValue(): string
    {
        if ($this->hashedValue !== null) {
            return $this->hashedValue;
        }

        if ($this->value === null) {
            throw new \LogicException('Cannot hash password without raw value');
        }

        return $this->hashedValue = \password_hash($this->value, self::ALGORITHM);
    }

    public function jsonSerialize(): string
    {
        return $this->getHashedValue();
    }

    public static function createFromJson(string $value): self
    {
        $class = new \ReflectionClass(self::class);
        $property = $class->getProperty('hashedValue');

        /** @var self $instance */
        $instance = $class->newInstanceWithoutConstructor();

        $property->setAccessible(true);
        $property->setValue($instance, $value);

        return $instance;
    }

    public function __toString(): string
    {
        return '********';
    }

    public function __construct(string $value)
    {
        $this->value = $value;
    }

    public function getValue(): string
    {
        if ($this->value === null) {
            throw new \LogicException('Cannot access raw value of a stored password');
        }

        return $this->value;
    }

    public function verify(string $password): bool
    {
        return \password_verify($password, $this->getHashedValue());
    }

    public function rehash(string $password): bool
    {
        if (!\password_needs_rehash($this->getHashedValue(), self::ALGORITHM)) {
            return false;
        }

        $this->hashedValue = \password_hash($password, self::ALGORITHM);

        return true;
    }
}

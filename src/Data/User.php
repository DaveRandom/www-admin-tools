<?php declare(strict_types = 1);

namespace DaveRandom\WwwAdminTools\Data;

final class User implements Entity
{
    private const DEFAULT_SHELL = '/sbin/nologin';

    private $username;
    private $password;
    private $homeDirectory;
    private $shell;

    public function jsonSerialize(): array
    {
        return [
            'username' => $this->username,
            'password' => $this->password,
            'home_directory' => $this->homeDirectory,
            'shell' => $this->shell,
        ];
    }

    public static function createFromJson(array $values): self
    {
        return new self(
            (string)$values['username'],
            Password::createFromJson($values['password']),
            (string)$values['home_directory'],
            (string)$values['shell']
        );
    }

    public function __toString(): string
    {
        return $this->username;
    }

    public function __construct(string $username, Password $password, string $homeDirectory, string $shell = self::DEFAULT_SHELL)
    {
        $this->username = $username;
        $this->homeDirectory = $homeDirectory;
        $this->password = $password;
        $this->shell = $shell;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function getPassword(): Password
    {
        return $this->password;
    }

    public function setPassword(Password $password): void
    {
        $this->password = $password;
    }

    public function getHomeDirectory(): string
    {
        return $this->homeDirectory;
    }

    public function getShell(): string
    {
        return $this->shell;
    }
}

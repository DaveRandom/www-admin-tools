<?php declare(strict_types = 1);

namespace DaveRandom\WwwAdminTools;

use DaveRandom\WwwAdminTools\Data\Host;

class DirectoryTreeCreator
{
    private $rootUid;
    private $webServerUid;

    public function __construct(string $rootUid = 'root', string $webServerUid = 'nginx')
    {
        $this->rootUid = $rootUid;
        $this->webServerUid = $webServerUid;
    }

    private function getUid(int $identifier, Host $domain): string
    {
        switch ($identifier) {
            case Owner::ROOT:       return $this->rootUid;
            case Owner::WEB_SERVER: return $this->webServerUid;
            case Owner::DOMAIN:     return $domain->getUser()->getUsername();
        }

        throw new \LogicException('Invalid user identifier: ' . $identifier);
    }

    private static function setPermissions(string $path, int $mode, string $uid, string $gid): bool
    {
        return \chmod($path, $mode) && \chown($path, $uid) && \chgrp($path, $gid);
    }

    public function createDirectoryTree(Host $domain): void
    {
        foreach ($domain->getDirectoryLayout() as $path => [$mode, $user, $group]) {
            if (!\mkdir($path, $mode, true)) {
                throw new \RuntimeException("Creating directory '{$path}' failed");
            }

            if (!self::setPermissions($path, $mode, $this->getUid($user, $domain), $this->getUid($group, $domain))) {
                throw new \RuntimeException("Setting permissions on '{$path}' failed");
            }
        }
    }
}

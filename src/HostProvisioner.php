<?php declare(strict_types = 1);

namespace DaveRandom\WwwAdminTools;

use DaveRandom\WwwAdminTools\Data\Accessor as DataAccessor;
use DaveRandom\WwwAdminTools\Data\Host;
use DaveRandom\WwwAdminTools\Ssl\CertificateCreator;
use DaveRandom\WwwAdminTools\SystemUsers\Creator as UserCreator;

class HostProvisioner
{
    private $userCreator;
    private $directoryTreeCreator;
    private $nginxManager;
    private $sslCertificateCreator;
    private $dataAccessor;

    public function __construct(
        UserCreator $userCreator,
        DirectoryTreeCreator $directoryTreeCreator,
        NginxManager $nginxManager,
        CertificateCreator $sslCertificateCreator,
        DataAccessor $dataAccessor
    ) {
        $this->userCreator = $userCreator;
        $this->directoryTreeCreator = $directoryTreeCreator;
        $this->nginxManager = $nginxManager;
        $this->sslCertificateCreator = $sslCertificateCreator;
        $this->dataAccessor = $dataAccessor;
    }

    private function progress(bool $show, string $message)
    {
        if ($show) {
            echo $message;
        }
    }

    public function provision(Host $host, bool $showProgress)
    {
        try {
            $this->progress($showProgress, "Provisioning host {$host}...\n");

            $this->progress($showProgress, "Creating user {$host->getUser()}...");
            $this->userCreator->create($host->getUser());
            $this->progress($showProgress, " OK\n");

            $this->progress($showProgress, "Creating directory tree under {$host->getBaseDirectory()}...");
            $this->directoryTreeCreator->createDirectoryTree($host);
            $this->progress($showProgress, " OK\n");

            $this->progress($showProgress, "Generating nginx HTTP config...");
            $this->nginxManager->generateHttpConfig($host);
            $this->nginxManager->reloadConfig();
            $this->progress($showProgress, " OK\n");

            $this->progress($showProgress, "Generating SSL certificate...");
            $certificate = $this->sslCertificateCreator->createSslCertificate($host);
            $this->progress($showProgress, " OK\n");

            $this->progress($showProgress, "Generating nginx HTTPS config...");
            $this->nginxManager->generateHttpsConfig($host, $certificate);
            $this->nginxManager->reloadConfig();
            $this->progress($showProgress, " OK\n");

            $this->progress($showProgress, "Storing host data...");
            $this->dataAccessor->storeHost($host);
            $this->progress($showProgress, " OK\n");
        } catch (\Throwable $e) {
            echo " Failed!\n";
            throw $e;
        }
    }
}

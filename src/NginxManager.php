<?php declare(strict_types = 1);

namespace DaveRandom\WwwAdminTools;

use DaveRandom\WwwAdminTools\Data\Host;
use DaveRandom\WwwAdminTools\Ssl\Certificate;

class NginxManager
{
    private const CONF_D_DIRECTORY = '/etc/nginx/conf.d';

    private $httpConfigTemplate;
    private $sslConfigTemplate;
    private $sslAliasConfigTemplate;

    private function getTemplateVars(Host $domain): array
    {
        return [
            'HOST'        => $domain->getPrimaryDomain(),
            'HOSTS'       => \implode(' ', $domain->getAllDomains()),
            'ALIAS_HOSTS' => \implode(' ', $domain->getAliasDomains()),
            'CONF_DIR'    => $domain->getConfDirectory(),
            'ROOT_DIR'    => $domain->getDocRootDirectory(),
            'LOGS_DIR'    => $domain->getLogsDirectory(),
        ];
    }

    private function getLocalConfPath(Host $domain): string
    {
        return $domain->getConfDirectory() . '/nginx.conf';
    }

    private function getSystemConfPath(Host $domain): string
    {
        return self::CONF_D_DIRECTORY . '/' . $domain->getPrimaryDomain() . '.conf';
    }

    private function createConfSymlinkIfNecessary($domain)
    {
        $systemPath = $this->getSystemConfPath($domain);

        if (!\file_exists($systemPath)) {
            \symlink(\realpath($this->getLocalConfPath($domain)), $systemPath);
        }
    }

    private function appendConfigData(Host $domain, string $config): void
    {
        \file_put_contents($this->getLocalConfPath($domain), $config, FILE_APPEND);
    }

    public function __construct()
    {
        $this->httpConfigTemplate = Template::createFromFile(DATA_ROOT . '/nginx.conf');
        $this->sslConfigTemplate = Template::createFromFile(DATA_ROOT . '/nginx.ssl.conf');
        $this->sslAliasConfigTemplate = Template::createFromFile(DATA_ROOT . '/nginx.ssl-aliases.conf');
    }

    public function generateHttpConfig(Host $domain)
    {
        $config = $this->httpConfigTemplate->render($this->getTemplateVars($domain));
        $this->appendConfigData($domain, $config);
        $this->createConfSymlinkIfNecessary($domain);
    }

    public function generateHttpsConfig(Host $domain, Certificate $certificate)
    {
        $templateVars = [
            'SSL_CERT_FILE' => $certificate->getCertificatePath(),
            'SSL_KEY_FILE' => $certificate->getKeyPath(),
        ] + $this->getTemplateVars($domain);

        $config = "\n" . $this->sslConfigTemplate->render($templateVars);

        if (\count($domain->getAliasDomains()) > 0) {
            $config .= "\n" . $this->sslAliasConfigTemplate->render($templateVars);
        }

        $this->appendConfigData($domain, $config);
    }

    public function restart()
    {
        \exec('service nginx restart', $output, $code);

        if ($code !== 0) {
            throw new \RuntimeException('Failed to restart nginx service');
        }
    }

    public function reloadConfig()
    {
        \exec('service nginx reload', $output, $code);

        if ($code !== 0) {
            throw new \RuntimeException('Failed to reload nginx service');
        }
    }
}

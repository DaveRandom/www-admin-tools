<?php declare(strict_types=1);

namespace DaveRandom\WwwAdminTools;

final class Owner
{
    public const ROOT = 1;
    public const WEB_SERVER = 2;
    public const DOMAIN = 3;

    private function __construct() { }
}

<?php declare(strict_types=1);

namespace DaveRandom\WwwAdminTools\Password;

use DaveRandom\WwwAdminTools\Data\Accessor as DataAccessor;
use DaveRandom\WwwAdminTools\Data\User;

class Accessor
{
    private $dataAccessor;

    public function __construct(DataAccessor $dataAccessor)
    {
        $this->dataAccessor = $dataAccessor;
    }

    public function verifyPassword(string $username, string $password): bool
    {
        return $this->dataAccessor->transaction(\LOCK_EX, function() use($username, $password) {
            if (!$this->dataAccessor->userExists($username)) {
                return false;
            }

            $user = $this->dataAccessor->getUser($username);

            if (!$user->getPassword()->verify($password)) {
                return false;
            }

            if ($user->getPassword()->rehash($password)) {
                $this->dataAccessor->storeUser($user);
            }

            return true;
        });
    }

    public function setSystemUserPassword(User $user): void
    {
        $command = \sprintf('passwd %s --stdin', $user->getUsername());
        $descriptorSpec = [['pipe', 'r'], ['pipe', 'w'], ['pipe', 'w']];

        $handle = \proc_open($command, $descriptorSpec, $pipes);

        if ($handle === false) {
            throw new \RuntimeException("Failed to set password for user '{$user->getUsername()}': starting passwd failed");
        }

        $passwordValue = $user->getPassword()->getValue();

        if (\fwrite($pipes[0], $passwordValue) !== \strlen($passwordValue)) {
            throw new \RuntimeException("Failed to set password for user '{$user->getUsername()}': sending data to passwd failed");
        }

        \fclose($pipes[0]);

        \stream_get_contents($pipes[1]);
        \fclose($pipes[1]);

        $success = '' === \stream_get_contents($pipes[2]);
        \fclose($pipes[2]);

        \proc_close($handle);

        if (!$success) {
            throw new \RuntimeException("Failed to set password for user '{$user->getUsername()}': passwd failed");
        }
    }
}

<?php declare(strict_types = 1);

namespace DaveRandom\WwwAdminTools\Password;

use DaveRandom\WwwAdminTools\Data\Password;

class Generator
{
    private const DEFAULT_LENGTH = 25;
    private const RANDOM_CHAR_MIN = 33;
    private const RANDOM_CHAR_MAX = 126;

    private $defaultLength;

    public function __construct(int $defaultLength = self::DEFAULT_LENGTH)
    {
        $this->defaultLength = $defaultLength;
    }

    public function generate(int $length = null): Password
    {
        $result = '';
        $length = $length ?? $this->defaultLength;

        for ($i = 0; $i < $length; $i++) {
            $result .= \chr(\random_int(self::RANDOM_CHAR_MIN, self::RANDOM_CHAR_MAX));
        }

        return new Password($result);
    }
}

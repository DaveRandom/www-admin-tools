<?php declare(strict_types=1);

namespace DaveRandom\WwwAdminTools\Ssl;

final class Certificate
{
    private $certificatePath;
    private $keyPath;

    public function __construct(string $certificatePath, string $keyPath)
    {
        $this->certificatePath = $certificatePath;
        $this->keyPath = $keyPath;
    }

    public function getCertificatePath(): string
    {
        return $this->certificatePath;
    }

    public function getKeyPath(): string
    {
        return $this->keyPath;
    }
}

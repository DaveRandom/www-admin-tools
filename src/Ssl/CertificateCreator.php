<?php declare(strict_types = 1);

namespace DaveRandom\WwwAdminTools\Ssl;

use DaveRandom\WwwAdminTools\Data\Host;

class CertificateCreator
{
    private const BASE_CMD = 'certbot-auto certonly --webroot';
    private const CERTS_LIVE_BASE = '/etc/letsencrypt/live';

    public function createSslCertificate(Host $domain): Certificate
    {
        $cmd = self::BASE_CMD
            . ' -w '. \escapeshellarg($domain->getDocRootDirectory())
            . ' -d ' . \escapeshellarg($domain->getPrimaryDomain());

        foreach ($domain->getAliasDomains() as $host) {
            $cmd .= ' -d ' . \escapeshellarg($host);
        }

        \exec($cmd, $output, $code);

        if ($code !== 0) {
            throw new \RuntimeException("Unable to create SSL certificate");
        }

        $certsBase = self::CERTS_LIVE_BASE . '/' . $domain->getPrimaryDomain();
        return new Certificate($certsBase . '/fullchain.pem', $certsBase . '/privkey.pem');
    }
}

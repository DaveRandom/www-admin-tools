<?php declare(strict_types = 1);

namespace DaveRandom\WwwAdminTools\SystemUsers;

use DaveRandom\WwwAdminTools\Data\User;
use DaveRandom\WwwAdminTools\Password\Accessor as PasswordAccessor;
use DaveRandom\WwwAdminTools\Template;

class Creator
{
    private const USERADD_COMMAND_TEMPLATE = 'useradd -M -d {HOME} -s {SHELL} {NAME}';

    private $passwordAccessor;
    private $useraddCommandTemplate;

    public function __construct(PasswordAccessor $passwordAccessor)
    {
        $this->passwordAccessor = $passwordAccessor;

        $this->useraddCommandTemplate = new Template(self::USERADD_COMMAND_TEMPLATE);
    }

    private function addSystemUser(string $name, string $homeDirectory, string $shell): void
    {
        \exec($this->useraddCommandTemplate->render([
            'NAME' => $name,
            'HOME' => $homeDirectory,
            'SHELL' => $shell,
        ]), $output, $code);

        if ($code !== 0) {
            throw new \RuntimeException("Failed to create user account '{$name}'");
        }
    }

    public function create(User $user): void
    {
        if ('' === (string)$user->getUsername()) {
            throw new \LogicException('User name cannot be empty');
        }

        if ('' === (string)$user->getShell()) {
            throw new \LogicException('Shell cannot be empty');
        }

        if ('' === (string)$user->getHomeDirectory()) {
            throw new \LogicException('Home directory cannot be empty');
        }

        $this->addSystemUser($user->getUsername(), $user->getHomeDirectory(), $user->getShell());
        $this->passwordAccessor->setSystemUserPassword($user);
    }
}

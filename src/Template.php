<?php declare(strict_types = 1);

namespace DaveRandom\WwwAdminTools;

class Template
{
    private const EXPR_TEMPLATE = '(%1$s%1$s(?=%1$s*+{)|%1$s{|{(\w+)})';
    private const DEFAULT_ESCAPE_CHAR = '@';

    private $template;
    private $escapeChar;

    private $expr;

    private static function makeCallback(array $variables, ?string $errPlaceholder, string $escapeChar): callable
    {
        $specials = [
            $escapeChar . $escapeChar => $escapeChar,
            $escapeChar . '{' => '{',
        ];

        return static function($match) use($variables, $errPlaceholder, $specials) {
            return $specials[$match[0]]
                ?? $variables[$match[1]]
                ?? $errPlaceholder
                ?? $match[0];
        };
    }

    public static function createFromFile(string $filePath, string $escapeChar = self::DEFAULT_ESCAPE_CHAR): self
    {
        $template = \file_get_contents($filePath);

        if ($template === false) {
            throw new \InvalidArgumentException("Unable to load template data from file: {$filePath}");
        }

        return new self($template, $escapeChar);
    }

    public function __construct(string $template, string $escapeChar = self::DEFAULT_ESCAPE_CHAR)
    {
        $this->template = $template;
        $this->escapeChar = $escapeChar;

        $this->expr = \sprintf(self::EXPR_TEMPLATE, \preg_quote($escapeChar));
    }

    public function render(array $variables, string $errPlaceholder = null): string
    {
        return \preg_replace_callback(
            $this->expr,
            self::makeCallback($variables, $errPlaceholder, $this->escapeChar),
            $this->template
        );
    }
}

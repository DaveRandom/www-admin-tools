<?php declare(strict_types = 1);

namespace DaveRandom\WwwAdminTools;

const DEFAULT_HOSTS_BASE_DIRECTORY = '/srv/www';

const APP_ROOT = __DIR__ . '/..';
const DATA_ROOT = __DIR__ . '/../data';

for ($i = 0; isset($argv[$i]); $i++) {
    if ($argv[$i] === '--serialize-output') {
        \define(__NAMESPACE__ . '\\SERIALIZE_OUTPUT', true);
        \array_splice($argv, $i, 1);
    }
}

if (!\defined(__NAMESPACE__ . '\\SERIALIZE_OUTPUT')) {
    /** @noinspection PhpConstantReassignmentInspection */
    \define(__NAMESPACE__ . '\\SERIALIZE_OUTPUT', false);
}

if (\file_exists(APP_ROOT . '/vendor/autoload.php')) {
    require APP_ROOT . '/vendor/autoload.php';
} else if (\file_exists(APP_ROOT . '/../../autoload.php')) {
    /** @noinspection PhpIncludeInspection */
    require APP_ROOT . '/../../autoload.php';
} else {
    $message = \DaveRandom\WwwAdminTools\SERIALIZE_OUTPUT
        ? \serialize('Unable to locate autoloader script')
        : 'Unable to locate autoloader script';

    \fwrite(\STDERR, "{$message}\n");
    exit(1);
}

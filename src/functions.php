<?php declare(strict_types = 1);

namespace DaveRandom\WwwAdminTools;

use Auryn\Injector;
use DaveRandom\WwwAdminTools\Data\Accessor as DataAccessor;
use DaveRandom\WwwAdminTools\Password\Accessor as PasswordAccessor;
use DaveRandom\WwwAdminTools\Password\Generator as PasswordGenerator;
use DaveRandom\WwwAdminTools\Ssl\CertificateCreator;

const DNS_NAME_EXPR = '(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-]*[a-zA-Z0-9])?)\.)*(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-]*[a-zA-Z0-9])?)';

function bootstrap(Injector $injector = null): Injector
{
    static $instance;

    return $instance ?? $instance = ($injector ?? new Injector())
        ->share(DataAccessor::class)
        ->share(PasswordAccessor::class)
        ->share(PasswordGenerator::class)
        ->share(CertificateCreator::class)
        ->share(DirectoryTreeCreator::class)
        ->share(HostProvisioner::class)
        ->share(NginxManager::class)
    ;
}

function error_exit($message, int $code = 1)
{
    if ($message instanceof \Throwable) {
        $message = \DaveRandom\WwwAdminTools\SERIALIZE_OUTPUT
            ? \serialize($message)
            : "A fatal error occurred: {$message->getMessage()}";
    } else {
        $message = \DaveRandom\WwwAdminTools\SERIALIZE_OUTPUT
            ? \serialize($message)
            : (string)$message;
    }

    \fwrite(\STDERR, \rtrim($message) . "\n");

    exit($code);
}

function is_valid_dns_name(string $subject): bool
{
    return \preg_match('(^' . \DaveRandom\WwwAdminTools\DNS_NAME_EXPR . '$)', $subject) === 1;
}

function guess_realpath(string $path, string $separator = \DIRECTORY_SEPARATOR): string
{

    if (false !== $real = \realpath($path)) {
        return $real;
    }

    $parts = \preg_split('#[\\\\/]+#', $path);

    if ($parts[0] === '' || (\stripos(\PHP_OS, 'win') === 0 && \preg_match('#^[A-Z]:$#i', $parts[0]))) {
        $real = [$parts[0]];
        $parts = \array_slice($parts, 1);
    } else {
        $real = \preg_split('#[\\\\/]+#', \rtrim(\getcwd(), '\\/'));
    }

    foreach ($parts as $part) {
        if ($part === '..' && \count($real) > 1) {
            array_pop($real);
        } else if ($part !== '.') {
            $real[] = $part;
        }
    }

    return \implode($separator, $real);
}

function check_password_complexity(string $password): bool
{
    return \strlen($password) > 20;
}
